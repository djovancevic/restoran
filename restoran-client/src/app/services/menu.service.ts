import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MenuItem } from '../model/menu-item';
import { Page } from '../model/page';
import { Observable } from 'rxjs';
import { filter } from 'minimatch';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private client: HttpClient) { }

  public getMenuPage(filter: string = "", page: number | null = null) : Observable<Page<MenuItem>> {
    var options = {params: {}};
    if(page !== null) {
      options["params"]["page"] = ""+(page-1);
    }

    if(filter !== null) {
      options["params"]["name"] = filter;
    }

    return this.client.get<Page<MenuItem>>("http://localhost:8080/api/menuItems", options)
  }

  public save(item: MenuItem) : Observable<MenuItem> {
    var url = "api/menuItems"
    if(item.id) {
      url += `/${item.id}`;
      return this.client.put<MenuItem>(url, item);
    } else {
      return this.client.post<MenuItem>(url, item);
    }
  }

  public delete(id: number) {
    return this.client.delete(`api/menuItems/${id}`);
  }
}
