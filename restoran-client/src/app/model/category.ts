import { MenuItem } from './menu-item';

export interface Category {
    id: number,
    name: string,
    menuItems?: MenuItem[];
}