import { Category } from './category';

export interface MenuItem {
    id: number,
    name: string,
    price: number,
    category: Category
}