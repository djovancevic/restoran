import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from '../model/menu-item';
import { MenuService } from '../services/menu.service';
import { MenuPreviewComponent } from '../menu-preview/menu-preview.component';
import { Action } from '../model/action';

@Component({
  selector: 'app-menu-editor',
  templateUrl: './menu-editor.component.html',
  styleUrls: ['./menu-editor.component.css']
})
export class MenuEditorComponent implements OnInit {
  @ViewChild("preview", {static: false})
  private preview: MenuPreviewComponent;

  selectedItem: MenuItem = {id: null, name: "", category: {id: null, name:""}, price: 0};

  constructor(private menuService: MenuService) { }

  ngOnInit() {
  }

  handleActions(action: Action<MenuItem>) {
    if(action.type == "delete") {
      this.deleteItem(action.data);
    } else if(action.type == "update") {
      this.editItem(action.data);
    }
  }

  saveItem(item: MenuItem) {
    this.menuService.save(item).subscribe(() => {
      this.preview.refreshMenu();
    });
  }

  editItem(item: MenuItem) {
    this.selectedItem = Object.assign({}, item);
  }

  deleteItem(item: MenuItem) {
    this.menuService.delete(item.id).subscribe(() => {
      this.preview.refreshMenu();
    })
  }
}
