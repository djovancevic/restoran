import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MenuItem } from '../model/menu-item';
import { Category } from '../model/category';
import { CategoryService } from '../services/category.service';

@Component({
  selector: 'app-menu-item-form',
  templateUrl: './menu-item-form.component.html',
  styleUrls: ['./menu-item-form.component.css']
})
export class MenuItemFormComponent implements OnInit {
  @Input("item")
  item: MenuItem = {id: null, name: "", category: {id: 1, name:"pizza"}, price: 0};

  @Output("onSave")
  onSave: EventEmitter<MenuItem> = new EventEmitter<MenuItem>();

  categories: Category[] = [];

  constructor(private categoryService: CategoryService) { }

  ngOnInit() {
    this.categoryService.getCategory().subscribe(categories => {
      this.categories = categories;
      this.clearForm();
    });
  }

  categoryId(index: number, category: Category) {
    return category.id;
  }


  public clearForm() {
    this.item = {id: null, name: "", category: {id: null, name:""}, price: 0};
  }

  public saveItem() {
    this.onSave.next(this.item);
    this.clearForm();
  }
}
