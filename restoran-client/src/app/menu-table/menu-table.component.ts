import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuItem } from '../model/menu-item';
import { Action } from '../model/action';

@Component({
  selector: 'app-menu-table',
  templateUrl: './menu-table.component.html',
  styleUrls: ['./menu-table.component.css']
})
export class MenuTableComponent implements OnInit {
  @Input("items")
  items: MenuItem[] = [];

  @Input("showActions")
  showActions: boolean = false;

  @Output("onAction")
  private onAction: EventEmitter<Action<MenuItem>> = new EventEmitter<Action<MenuItem>>();

  constructor() { }

  ngOnInit() {
  }

  delete(item: MenuItem) {
    this.onAction.next({type: "delete", data: item});
  }

  update(item: MenuItem) {
    this.onAction.next({type: "update", data: item});
  }
}
