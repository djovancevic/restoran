import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-menu-search-form',
  templateUrl: './menu-search-form.component.html',
  styleUrls: ['./menu-search-form.component.css']
})
export class MenuSearchFormComponent implements OnInit {
  filter: string = "";

  @Output("onSearch")
  private onSearch: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  search() {
    this.onSearch.next(this.filter);
  }
}
