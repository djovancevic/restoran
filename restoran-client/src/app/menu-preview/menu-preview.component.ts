import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MenuService } from '../services/menu.service';
import { Action } from '../model/action';
import { MenuItem } from '../model/menu-item';
import { Page } from '../model/page';

@Component({
  selector: 'app-menu-preview',
  templateUrl: './menu-preview.component.html',
  styleUrls: ['./menu-preview.component.css']
})
export class MenuPreviewComponent implements OnInit {
  @Input("showActions")
  showActions: boolean = false;

  @Output("onAction")
  private onAction: EventEmitter<Action<MenuItem>> = new EventEmitter<Action<MenuItem>>();

  menuPage: Page<MenuItem> = {currentPage: 0, itemsPerPage: 0, totalItems: 0, items: []}
  private filter: string = "";
  private page: number = 1;
  constructor(private menuService: MenuService) { }

  ngOnInit() {
    this.refreshMenu();
  }

  public setFilter(filter: string) {
    this.filter = filter;
    this.refreshMenu();
  }

  public setPage(page: number) {
    this.page = page;
    this.refreshMenu();
  }

  public refreshMenu(filter: string = "", page: number = 1) {
    this.menuService.getMenuPage(this.filter, this.page).subscribe(menuPage => {
      menuPage.currentPage += 1;
      this.menuPage = menuPage;
    })
  }

  action(action: Action<MenuItem>) {
    this.onAction.next(action);
  }
}
