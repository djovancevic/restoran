import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuTableComponent } from './menu-table/menu-table.component';
import { MenuItemFormComponent } from './menu-item-form/menu-item-form.component';
import { MenuSearchFormComponent } from './menu-search-form/menu-search-form.component';
import { FormsModule } from '@angular/forms'
import { HttpClientModule } from '@angular/common/http';
import { NgbPaginationModule }  from '@ng-bootstrap/ng-bootstrap';
import { MenuEditorComponent } from './menu-editor/menu-editor.component';
import { MenuPreviewComponent } from './menu-preview/menu-preview.component'

@NgModule({
  declarations: [
    AppComponent,
    MenuTableComponent,
    MenuItemFormComponent,
    MenuSearchFormComponent,
    MenuEditorComponent,
    MenuPreviewComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbPaginationModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
