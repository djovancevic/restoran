import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { MenuPreviewComponent } from './menu-preview/menu-preview.component';
import { MenuEditorComponent } from './menu-editor/menu-editor.component';


const routes: Routes = [
  {path: "admin", component: MenuEditorComponent},
  {path: "**", component: MenuPreviewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
