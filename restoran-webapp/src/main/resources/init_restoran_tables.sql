use restoran;

insert into category (  name) values ( 'predjelo');
insert into category (  name) values ( 'glavno jelo');
insert into category (  name) values ( 'jelo sa rostilja');
insert into category (  name) values ( 'dezert');
insert into category (  name) values ( 'sok');
insert into category (  name) values ( 'alkoholno pice');


insert into menu_item (  name, price, category_id) values (  'slanina', 50, 1);
insert into menu_item (  name, price, category_id) values (  'sunka', 150, 1);
insert into menu_item (  name, price, category_id) values (  'kozji sir', 60, 1);
insert into menu_item (  name, price, category_id) values (  'pasulj', 250, 2);
insert into menu_item (  name, price, category_id) values (  'paprikas', 350, 2);
insert into menu_item (  name, price, category_id) values (  'musaka', 350, 2);
insert into menu_item (  name, price, category_id) values (  'pecenje', 450, 2);
insert into menu_item (  name, price, category_id) values (  'smudj', 450, 2);
insert into menu_item (  name, price, category_id) values (  'cevapi juneci', 450, 3);
insert into menu_item (  name, price, category_id) values (  'biftek', 750, 3);
insert into menu_item (  name, price, category_id) values (  'cheese cake', 150, 4);
insert into menu_item (  name, price, category_id) values (  'palacinke', 150, 4);
insert into menu_item (  name, price, category_id) values (  'coca-cola', 90, 5);
insert into menu_item (  name, price, category_id) values (  'heineken', 100, 6);




