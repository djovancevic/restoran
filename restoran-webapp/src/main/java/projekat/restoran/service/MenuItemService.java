package projekat.restoran.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import projekat.restoran.data.MenuItemRepository;
import projekat.restoran.model.MenuItem;

@Service
public class MenuItemService {
	@Autowired
	MenuItemRepository mir;
	
	public MenuItemService() {
		super();
	}
	
	public MenuItem findOne(Long id) {
		return mir.findById(id).get();
	}
	
	public Page<MenuItem> getMenuItems(Pageable pageable) {
		return mir.findAll(pageable);
	}
	
	public Page<MenuItem> getMenuItems(String name, Pageable pageable) {
		return mir.findMenuItemByNameContains(name, pageable);
	}
	
	public MenuItem save(MenuItem menuItem) {
		return mir.save(menuItem);
	}
	
	public void delete(Long id) {
		mir.deleteById(id);
	}
}
