package projekat.restoran.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import projekat.restoran.data.CategoryRepository;
import projekat.restoran.model.Category;

@Service
public class CategoryService {
	@Autowired
	CategoryRepository cr;
	
	public CategoryService() {
		super();
	}
	
	public List<Category> getCategories() {
		return cr.findAll();
	}
	
	public Page<Category> getCategories(Pageable pageable) {
		return cr.findAll(pageable);
	}
	
	public Category findOne(Long id) {
		return cr.findById(id).orElse(null);
	}
}
