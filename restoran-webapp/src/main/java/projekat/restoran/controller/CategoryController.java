package projekat.restoran.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import projekat.restoran.dto.CategoryDTO;
import projekat.restoran.model.Category;
import projekat.restoran.service.CategoryService;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping(value="/api/categories")
public class CategoryController {
	@Autowired
	CategoryService cs;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<CategoryDTO>> getCategories() {
		List<CategoryDTO> categories = new ArrayList<CategoryDTO>();
		for(Category c : cs.getCategories()) {
			categories.add(new CategoryDTO(c));
		}
		
		return new ResponseEntity<List<CategoryDTO>>(categories, HttpStatus.OK);
	}
}
