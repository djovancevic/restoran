package projekat.restoran.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import projekat.restoran.dto.CategoryDTO;
import projekat.restoran.dto.MenuItemDTO;
import projekat.restoran.dto.PageDTO;
import projekat.restoran.model.Category;
import projekat.restoran.model.MenuItem;
import projekat.restoran.service.CategoryService;
import projekat.restoran.service.MenuItemService;

@Controller
@CrossOrigin("http://localhost:4200")
@RequestMapping(value="/api/menuItems")
public class MenuItemController {
	@Autowired
	MenuItemService mis;
	
	@Autowired
	CategoryService cs;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<PageDTO<MenuItemDTO>> getMenuItems(@RequestParam(required=false, defaultValue="") String name, Pageable pageable) {
		List<MenuItemDTO> menuItems = new ArrayList<MenuItemDTO>();
		Page<MenuItem> page = mis.getMenuItems(name, pageable);
		for(MenuItem mi : page.getContent()) {
			MenuItemDTO midto = new MenuItemDTO(mi);
			midto.setCategory(new CategoryDTO(mi.getCategory()));
			menuItems.add(midto);
		}
		
		PageDTO<MenuItemDTO> pageDto = new PageDTO<MenuItemDTO>(pageable.getPageNumber(),
				pageable.getPageSize(), page.getTotalElements(), menuItems);
		
		return new ResponseEntity<PageDTO<MenuItemDTO>>(pageDto, HttpStatus.OK);
	}

	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<MenuItemDTO> createMenuItem(@RequestBody MenuItemDTO menuItemDto) {
		Category category = cs.findOne(menuItemDto.getCategory().getId());
		MenuItem newItem = new MenuItem(null, menuItemDto.getName(), category, menuItemDto.getPrice());
		newItem = mis.save(newItem);
		
		return new ResponseEntity<MenuItemDTO>(new MenuItemDTO(newItem), HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<MenuItemDTO> updateMenuItem(@PathVariable Long id, @RequestBody MenuItemDTO menuItemDto) {
		Category category = cs.findOne(menuItemDto.getCategory().getId());
		MenuItem menuItem = mis.findOne(id);
		menuItem.setCategory(category);
		menuItem.setName(menuItemDto.getName());
		menuItem.setPrice(menuItemDto.getPrice());
		menuItem = mis.save(menuItem);
		return new ResponseEntity<MenuItemDTO>(new MenuItemDTO(menuItem), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Object> deleteMenuItem(@PathVariable Long id) {
		mis.delete(id);
		return new ResponseEntity<Object>(HttpStatus.NO_CONTENT);
	}
}
