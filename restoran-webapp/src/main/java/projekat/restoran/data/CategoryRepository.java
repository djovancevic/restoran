package projekat.restoran.data;

import org.springframework.data.jpa.repository.JpaRepository;

import projekat.restoran.model.Category;

public interface CategoryRepository extends JpaRepository<Category, Long>{
}
