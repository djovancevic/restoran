package projekat.restoran.data;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import projekat.restoran.model.MenuItem;

public interface MenuItemRepository extends JpaRepository<MenuItem, Long> {
	Page<MenuItem> findMenuItemByNameContains(String name, Pageable pageable);
}
