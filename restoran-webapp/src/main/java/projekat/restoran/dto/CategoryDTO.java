package projekat.restoran.dto;

import java.util.HashSet;
import java.util.Set;

import projekat.restoran.model.Category;

public class CategoryDTO {
	private Long id;
	private String name;
	private Set<MenuItemDTO> menuItems = new HashSet<MenuItemDTO>();
	
	public CategoryDTO() {
		super();
	}
	
	public CategoryDTO(Category category) {
		this.id = category.getId();
		this.name = category.getName();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<MenuItemDTO> getMenuItems() {
		return menuItems;
	}

	public void setMenuItems(Set<MenuItemDTO> menuItems) {
		this.menuItems = menuItems;
	}
}
