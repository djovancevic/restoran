package projekat.restoran.dto;

import projekat.restoran.model.MenuItem;

public class MenuItemDTO {
	private Long id;
	private String name;
	private CategoryDTO category;
	private Double price;
	
	public MenuItemDTO() {
		super();
	}
	
	public MenuItemDTO(MenuItem menuItem) {
		this.id = menuItem.getId();
		this.name = menuItem.getName();
		this.price = menuItem.getPrice();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CategoryDTO getCategory() {
		return category;
	}

	public void setCategory(CategoryDTO category) {
		this.category = category;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
}
